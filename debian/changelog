blends (0.7.4) UNRELEASED; urgency=medium

  * Add Ubuntu disco and eoan sources.list files

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 14 Jun 2019 00:58:11 +0200

blends (0.7.3) experimental; urgency=medium

  [ Gianfranco Costamagna ]
  * Add Ubuntu sources.lists

  [ Andreas Tille ]
  * Do not assume that short name is behind a '-' but strip only '^debian-' to
    obtain short name
  * blends-common Depends: sensible-utils
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Sat, 09 Mar 2019 15:13:00 +0100

blends (0.7.2) unstable; urgency=medium

  [ Andreas Tille ]
  * Replace bc call with a portable shell substitution (thanks to Lars Kruse
    for the patch)

  [ Wolfgang Schweer ]
  * python3-blends: shouldn't write Packages: list in udeb case
    (Closes: #903593)

 -- Ole Streicher <olebole@debian.org>  Fri, 13 Jul 2018 17:32:06 +0200

blends (0.7.1) unstable; urgency=medium

  * Depends: bc
    Closes: #903292
  * debhelper 11
  * Standards-Version: 4.1.5

 -- Andreas Tille <tille@debian.org>  Sun, 08 Jul 2018 17:32:33 +0200

blends (0.7.0) unstable; urgency=low

  [ Andreas Tille ]
  * Do not mention subversion any more
  * s/alioth/salsa/
  * Description how Blends relevant data are gathered and stored
  * Standards-Version: 4.1.4

  [ Ole Streicher ]
  * Switch back to unstable
  * Create and use python3-blends package
  * Move distribution name fix to blends-dev Makefile
  * blends.py: Several UDD query fixes
  * blends-gen-control: read defaults from GENCONTROL_OPTS env var
  * blend-gen-control: Improve program output

 -- Ole Streicher <olebole@debian.org>  Fri, 13 Apr 2018 20:40:52 +0200

blends (0.6.103) experimental; urgency=low

  [ Andreas Tille ]
  * d/control:
     - Build-Depends: dh-python, python3-all
     - blends-dev: Depends: ${python3:Depends}
  * d/rules: --with python3
  * ignore false lintian warning binary-package-depends-on-toolchain-package
  * cme fix dpkg-control
  * Take over tasks_diff from blends-gsoc to create dependency_data
  * Use Deb822List to evaluate taskprefix in tasks_diff

  [ Ole Streicher ]
  * Add UDD query options to blend-gen-control

 -- Ole Streicher <olebole@debian.org>  Thu, 29 Mar 2018 21:33:29 +0200

blends (0.6.102) experimental; urgency=low

  * blend-gen-control: Bugfixes, slight refactorization, documentation
     - Don't lower Recommends to Suggests in format 1.0
     - Fail on backslashes in dependency lists

 -- Ole Streicher <olebole@debian.org>  Wed, 28 Mar 2018 08:49:42 +0200

blends (0.6.101) experimental; urgency=low

  [ Andreas Tille ]
  * Moved to Salsa

  [ Ole Streicher ]
  * Switch to experimental
  * Replace blend-gen-control with a Python implementation
    - Degrade packages from contrib/non-free to Suggests even when enforcing
      strict depends. Closes: #891188
    - Do not drop virtual packages in alternatives. Closes: #785678
    - Use Python deb822 to parse tasks files.
    - Recognize Format field in tasks file header. Closes: #825161

 -- Ole Streicher <olebole@debian.org>  Sat, 24 Mar 2018 10:41:20 +0100

blends (0.6.100) unstable; urgency=medium

  * Team upload

  * devtools/blend-gen-control: Nasty typo fix for (= ${source:Version})
    expression (swap "$" and "{").
  * debian/control: Add myself to Uploaders: field.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 21 Aug 2017 14:11:25 -0400

blends (0.6.99) unstable; urgency=medium

  * Team upload

  [ Wookey ]
  * Fix english grammar in README.BLENDS

  [ Mike Gabriel ]
  * devtools/Makefile: Make -D option configurable from the make command
    call.
  * devtools/blend-gen-control: Enable Depends: _and_ Recommends: in task files
    end up under Recommends: in debian/control.
  * devtools/Makefile: Make -D option configurable from the make command call
    to enable real Depends in debian/control.  This can be set by
      GENCONTROL_DEPEND = true
    in Makefile (example: debian-edu src:package). (Closes: #825172).

  [ Andreas Tille ]
  * Do not parse changelog to get package metadata

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 12 Aug 2017 15:43:25 +0200

blends (0.6.98) unstable; urgency=medium

  * Team upload
  * devtools: White-space clean-up: Removal of tabs from Perl scripts,
    removal of trailing white-spaces.
  * Consolidate package name extraction even more, fix breakage for
    make dist run against debian-edu (v1.924 and earlier).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 05 Aug 2017 15:48:09 +0200

blends (0.6.97) unstable; urgency=medium

  [ Mike Gabriel ]
  * Team upload.
  * load_task: Improve header and package name scanning to enable no package
    right after "Depends:" and parse following line for packages

  [ Andreas Tille ]
  * Standards-Version: 4.0.0 (no changes needed)

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 05 Aug 2017 04:54:26 +0200

blends (0.6.96) unstable; urgency=medium

  * Use (= ${source:Version}) for arch:all depends
    Closes: #845760
  * add ${misc:Depends}
    Closes: #845762
  * debhelper 10

 -- Andreas Tille <tille@debian.org>  Sat, 26 Nov 2016 20:12:57 +0100

blends (0.6.95) unstable; urgency=medium

  [ Ole Streicher ]
  * Adjusted blend-gen-control to handle RFC822 style continuation
    lines as well as backslash style (Closes: #840094).

  [ Andreas Tille ]
  * Remove Mandriva from doc, since link was not active any more.

 -- Andreas Tille <tille@debian.org>  Sat, 26 Nov 2016 15:45:52 +0100

blends (0.6.94) unstable; urgency=medium

  [ Ole Streicher ]
  * Make the default install opt-in instead of opt-out, and create the
    <blends>-all package only when there were tasks opted-in as default
    install. Closes: 825004
  * Adjust descriptions for the installer's tasksel
  * Remove debian-science from the installer's tasksel

  [ Holger Levsen ]
  * Adjust description of Debian Edu: use correct project spelling and improve
    language.

  [ Andreas Tille ]
  * Apply patch by Wolfgang Schweer <wschweer@arcor.de> to support udebs using
    blends (thanks Wolfgang for the patch
    Closes: #835528
  * Bump Standards-Version
  * Fix typo in d/rules

 -- Ole Streicher <olebole@debian.org>  Mon, 12 Sep 2016 09:40:48 +0200

blends (0.6.93) unstable; urgency=medium

  [ Ole Streicher ]
  * Fix Relevance field for tasksel
  * Group tasks of one blend under a parent in tasksel
  * Update Standards-Version to 3.9.7: Change VCS-Git URL to https
  * New package blends-tasks. Closes: #758116

  [ Andreas Tille ]
  * Update list of Uploaders
  * Silencing `make dist` output
  * Create xz compressed source tarballs and make tar a bit more 'predictable'
  * Re-enable dist target in rules file
    Closes: #819425

 -- Ole Streicher <olebole@debian.org>  Sat, 09 Apr 2016 20:42:12 +0200

blends (0.6.92.5) unstable; urgency=medium

  [ Markus Koschany ]
  * do not install README.source into binary packages
    Closes: #799601

  [ Emmanouil Kiagias ]
  * documentation converted to XML in version 0.6.91 - closing the according
    bug here
    Closes: #612997

 -- Andreas Tille <tille@debian.org>  Sat, 02 Jan 2016 09:14:26 +0100

blends (0.6.92.4) unstable; urgency=medium

  * Drop Simple-CDD from docs
  * Document how to deal with name space pollution
  * devtools/blend-gen-control: Sorted llist of packages in tasksel control
    files to enable better comparison of the generation result

 -- Andreas Tille <tille@debian.org>  Tue, 01 Sep 2015 18:35:24 +0200

blends (0.6.92.3) unstable; urgency=medium

  * document that the task files follow RFC 822 format (thanks for the
    patch to Markus Koschany <apo@gambaru.de>)
    Closes: #764591
  * Fix bashism
    Closes: #772186
  * fix prerm file causes lintian warnings  (thanks for the patch to
    Markus Koschany <apo@gambaru.de>)
    Closes: #779212
  * Document further resources
    Closes: #783240
  * xmlto failed without --skip-validation - any help to fix XML is welcome
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Thu, 02 Jul 2015 17:49:27 +0200

blends (0.6.92.2+exp) experimental; urgency=medium

  * templates/profile.sh: Enable users to specify a Blend name in ~/.blends
    and prepend an according path to PATH
  * devtools/blend-install-helper: Install profile.d script to enable adjusting
    PATH for Blend users

 -- Andreas Tille <tille@debian.org>  Fri, 30 Jan 2015 14:14:03 +0100

blends (0.6.92.2) unstable; urgency=medium

  * devtools/blend-gen-control: Prevent respecting users sources.list.d dir
    Closes: #768011

 -- Andreas Tille <tille@debian.org>  Mon, 03 Nov 2014 11:28:06 +0100

blends (0.6.92.1) unstable; urgency=medium

  * Fix typo: s/Section: metapackage/&s/
  * Reformat debian/*.NEWS

 -- Andreas Tille <tille@debian.org>  Fri, 01 Aug 2014 08:14:08 +0200

blends (0.6.92) unstable; urgency=medium

  [ Andreas Tille ]
  * Updated doc
  * Force explicitly "Section: metapackage" to the metapackages except
    if something else than 'misc' was set.  For the reason of this change
    please see bug #720199
  * Reformat NEWS.Debian entry

  [ Franklin Weng ]
  * Added EzGo as existing Blend
  * Better documentation for non-metapackages

 -- Andreas Tille <tille@debian.org>  Mon, 28 Jul 2014 09:59:12 +0200

blends (0.6.91) unstable; urgency=low

  [ Emmanouil Kiagias ]
  * Documentation converted to XML

  [ Andreas Tille ]
  * debian/control: Adapt Build-Depends to XML version of docs
  * debtools/rules: Do not remove postinst scripts which are not
    autogenerated
  * cme fix dpkg-control:
     - Unversioned dependency from debconf and menu
     - Standatde-Version: 3.9.5
     - Canonical Vcs URLs
     - Drop Linux from "Debian Linux"

 -- Andreas Tille <tille@debian.org>  Wed, 26 Mar 2014 08:07:45 +0100

blends (0.6.16.4) unstable; urgency=low

  * devtools/blend-gen-control: Apply patch from Felipe Sateler
    <fsateler@debian.org> (thanks Felipe!) to separate package
    dependencies with a newline
    Closes: #709057

 -- Andreas Tille <tille@debian.org>  Tue, 21 May 2013 23:39:39 +0200

blends (0.6.16.3) unstable; urgency=low

  * devtools/rules: Better way to obtain version string
  * do not create preinst and postrm scripts any more because
    preinst is unneeded since Lenny and postrm was only introduced
    for completeness but never used
    Closes: #708820, #708879
  * templates/{preinst,postrm}: Removed because unneeded
  * debian/control
     - Standards-Version: 3.9.4 (no changes needed)
     - Debhelper 9 (also d/compat)
     - Removed duplicated Section: devel
     - normalised format
     - blends-dev also required debhelper version 9
  * debian/rules:
     - use dpkg-parsechangelog to obtain pkg name and version
     - use short dh syntax
  * debian/source/format: 3.0 (native)
  * debian/examples: also require debhelper 9 + latest blends-dev

 -- Andreas Tille <tille@debian.org>  Mon, 20 May 2013 10:10:53 +0200

blends (0.6.16.2) unstable; urgency=low

  * Install tasksel desc file to new location since version 3.00 of tasksel
    (thanks to Petter Reinholdtsen <pere@hungry.com> for the patch)
    Closes: #694896

 -- Andreas Tille <tille@debian.org>  Tue, 04 Dec 2012 11:00:45 +0100

blends (0.6.16.1) unstable; urgency=low

  * Revert bumping of debhelper and Standards-Version to match the
    status of 0.6.15 and thus comply with freeze policy
  * The fix to the sources.list* files to add the missing debian/ dirs
    is now documented in BTS and this changelog entry
    Closes: #693864

 -- Andreas Tille <tille@debian.org>  Wed, 21 Nov 2012 08:51:11 +0100

blends (0.6.16) unstable; urgency=low

  * Enhanced doc
  * sources.list/*: Add the missing debian/ dirs
  * debian/control:
     - Standards-Version: 3.9.3
     - Vcs-Fields now point to Git
  * Debhelper 9 (control+compat)
  * Drop transitional cdd-* packages completely
    Closes: #692946
  * DEP5 formated copyright while checking this in connection to
    bug #692946

 -- Andreas Tille <tille@debian.org>  Fri, 16 Nov 2012 14:31:49 +0100

blends (0.6.15) unstable; urgency=low

  * Documentation changes:
    - doc/en: Update about existing Blends
    - examples/control.stub:
      + Updated debhelper version, Standards-Version
      + Removed two useless lines at the end
  * devtools/rules: ignore .git directories when creating orig.tar.gz
  * templates/{postinst,prerm}: use /bin/sh instead of /bin/bash to
    be sure that the requested shell is really available (there is
    no explicite bash dependency enforced
  * templates/postrm: Provide code for failed-upgrade to enable
    smooth upgrades from Lenny
  * templates/preinst: Provide this template for completeness
  * devtools/{rules,Makefile}: clean targets will remove also postrm
    and preinst if existant

 -- Andreas Tille <tille@debian.org>  Wed, 10 Nov 2010 20:32:31 +0100

blends (0.6.14) unstable; urgency=low

  * doc/en: Updated documentation about webtools
  * devtools/rules: Disable dh_auto_build which tries to
    regenerate debian/control but should not
  * Standards-Version: 3.9.1 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Tue, 03 Aug 2010 10:03:45 +0200

blends (0.6.13) unstable; urgency=low

  * Do not use explicite PATH to blend-update-menus to avoid
    lintian warning in metapackages.

 -- Andreas Tille <tille@debian.org>  Sat, 20 Mar 2010 22:38:55 +0100

blends (0.6.12) unstable; urgency=low

  * Use prerm instead of postrm and verify existance of
    blends-common in this file to deal with problems like
    #574237, #574203, #574208

 -- Andreas Tille <tille@debian.org>  Wed, 17 Mar 2010 08:06:31 +0100

blends (0.6.11) unstable; urgency=low

  * Install NEWS.Debian files properly
  * Standards-Version: 3.8.4 (no changes needed)
  * Build-Depends: ghostscript (Thanks for the patch to
    Leo Iannacone <leo.iannacone@gmail.com>)
    Closes: #569802

 -- Andreas Tille <tille@debian.org>  Mon, 15 Feb 2010 08:40:54 +0100

blends (0.6.10) unstable; urgency=low

  * rm debian/blends-dev.preinst = do not try to take over config
    files from cdd-dev to blends-dev and warn in NEWS.Debian about this
    Closes: #562854

 -- Andreas Tille <tille@debian.org>  Wed, 27 Jan 2010 13:05:07 +0100

blends (0.6.9) unstable; urgency=low

  * Do not try to take over config files from /etc/cdd -
    it just leads to trouble and is not worth the effort
    because they are unchanged in most practical cases
    -> Remove debian/blends-common.preinst
  * Updated debian/blends-common.README.Debian
  * debian/control: Added ${misc:Depends} to all package
    dependencies

 -- Andreas Tille <tille@debian.org>  Mon, 11 Jan 2010 19:23:04 +0100

blends (0.6.8) unstable; urgency=low

  * templates/config.postinst: Really fix the problem of missing
    config files which has leaded to #550140 and #553632
  * debian/blends-common.preinst: Do config file moving only
    if it was not yet done

 -- Andreas Tille <tille@debian.org>  Mon, 09 Nov 2009 11:59:52 +0100

blends (0.6.7) unstable; urgency=low

  * Better documentation how to generate tasks pages
  * devtools/Makefile: Remove tasksel .desc and debian/control
    file in dist target to ensure that these will be created in
    any case
    Closes: #554261
  * devtools/rules: Do not remove the autogenerated files
    automatically in get-orig-source target because there are
    people who explicitly like to commit these to SVN
  * templates/config.postinst: Return true even if config file is
    missing.  In principle this should not happen, but there are
    two bug reports about this problem (#550140 and #553632) which
    will be fixed by this workaround.  Most probably this is a side
    effect from renaming.

 -- Andreas Tille <tille@debian.org>  Wed, 04 Nov 2009 22:25:05 +0100

blends (0.6.6) unstable; urgency=low

  * Fixing some links in the documentation which is addressed by bug
    #542385 but this bug is not solved by package upload but rather by
    updating the doc on the website
  * blends-common.preinst: Do not create config files from Blends
    guessing from old CDD configuration file.  The former behaviour to
    pre-build those config files leaded to #542656 and #542658.
  * Removed debian/changelog.med-common because there is no point in
    providing this now 5 year old file for ever.
  * Standards-Version: 3.8.3 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Fri, 21 Aug 2009 14:24:50 +0200

blends (0.6.5) unstable; urgency=low

  * remove minor debugging code from debian/rules
  * debian/blends-dev.preinst: make sure there are really
    old sources.list files to copy before trying to do this
    Closes: #540603

 -- Andreas Tille <tille@debian.org>  Sun, 09 Aug 2009 07:48:39 +0200

blends (0.6.4) unstable; urgency=low

  * devtools/blend-get-names: Enable Blends without prefix "debian-"
    (brdesktop, debichem)
  * Standards-Version: 3.8.2 (no changes needed)
  * Move to debhelper 7 short rules file when building Blends
  * Provide also sources.list.UNRELEASED to enable test builds
  * debian/rules: drop -k option from dh_clean
  * devtools/Makefile: Do not use distclean as target to remove
    automatically created control and tasksel file but rather
    the proper target.  Rationale: dh_clean calls make distclean
    which would remove debian/control which has to be prevented
    for sure.

 -- Andreas Tille <tille@debian.org>  Wed, 29 Jul 2009 10:40:04 +0200

blends (0.6.3) unstable; urgency=low

  * Section for cdd-common misc instead of devel
  * Updated mailing list address of Debian Pure Blends team
  * devtools/blends-gen-control:
     - If a task lists not a single package which is available in
       the target distribution the metapackage will be suppressed
       if option '-S' is set; there is one exception which is needed
       by Debian Edu: If Test-always-lang is set also empty tasks
       might be created
     - Prevent Suggestion of non policy conform package names
       containing upper case letters
  * devtools/Makefile:
    - Use -S option of blends-gen-control to not create
      metapackages with not a single available package
    - Set LC_ALL=C to prevent blends-gen-control from loading
      description translation files according to users environment

 -- Andreas Tille <tille@debian.org>  Fri, 10 Apr 2009 21:26:00 +0200

blends (0.6.2) unstable; urgency=low

  * Bumped policy to 3.8.1 and make use of the new feature that
    debian/control allows comment lines starting with # with no
    preceding whitespace. [Policy paragraph 5.2]
  * Fixed Vcs tags (s/cdd/blends/)

 -- Andreas Tille <tille@debian.org>  Thu, 02 Apr 2009 09:16:07 +0200

blends (0.6.1) experimental; urgency=low

  * Add sources.list.experimental
  * debian/rules: variable for package name
  * Remove explicite path /usr/sbin/ from blend-update-menus in
    templates/post{inst,rm} because lintian warns about it in the
    Blend metapackages according to Debian Policy Manual section
    6.1.
  * more elegant method to obtain package version from changelog

 -- Andreas Tille <tille@debian.org>  Fri, 30 Jan 2009 19:30:53 +0100

blends (0.6.0) experimental; urgency=low

  * The great renaming:  The confusing name Custom Debian Distribution
    was dropped after DebConf 8 and replaced by Debian Pure Blends -
    in short blends if the iDebian internal use is evident.  This
    reflects in a rename of the package and all the tools.
  * debian/copyright: new format

 -- Andreas Tille <tille@debian.org>  Sun, 04 Jan 2009 18:08:57 +0100

cdd (0.5.6) unstable; urgency=low

  * doc/Makefile: publish target now enables moving the docs to
    http://cdd.alioth.debian.org/cdd instead of
    people.debian.org/~tille/cdd
  * share/cdd/unixgroups/cdd-actions: Do not fail if there are
    NIS users on this machine.  These will just be ignored.
    Closes: #491680

 -- Andreas Tille <tille@debian.org>  Sun, 13 Jul 2008 13:45:32 +0200

cdd (0.5.5) unstable; urgency=low

  * New task header Metapackage to make it possible to disable the
    generation of a binary package with dependencies (and only get a
    tasksel task).

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 11 Jul 2008 22:08:11 +0200

cdd (0.5.4) unstable; urgency=low

  * Added patch from Petter Reinholdtsen <pere@hungry.com> to enable
    Enhances feature for tasksel.
    Closes: #489811
  * Better Debconf template for user menus which avoids lintian warnings
    in CDD metapackages.

 -- Andreas Tille <tille@debian.org>  Tue, 08 Jul 2008 10:40:42 +0200

cdd (0.5.3) unstable; urgency=low

  * Avoid rsync to build dist target
    Closes: #488262
  * Standards-Version: 3.8.0 (no changes necessary)
  * Make use of dh_lintian to install overrides
  * Lintian prefers spelling "metapackage" over meta package
    on the other hand my dictionary claims metapackage wrong
    and votes for meta-package.  I used the lintian clean
    spelling suggestion but we need some agreement about a
    consistent spelling ...

 -- Andreas Tille <tille@debian.org>  Mon, 30 Jun 2008 13:29:11 +0200

cdd (0.5.2) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Changed the Maintainer to Custom Debian Distribution Team
    ( s/List/Team/ )
  * Add myself as uploader.

  [ Andreas Tille ]
  * Debhelper (>= 6.0.7): Rationale: We are using dh_lintian which
    is only available from this version on.  This is actually not needed
    to build the cdd-common or cdd-dev package, but it makes perfectly
    sense to build with the same debhelper version as cdd-dev will
    finally depend from
  * share/menu/cdd-menu: verify that /etc/cdd/cdd.conf exists before
    sourcing it because since menu 2.1.39 supports triggers on
    /usr/{lib,share}/menu update-menus is called on every touch of
    these directory and because we have no safe way to ensure that
    /etc/cdd/cdd.conf is unpackaged before cdd-menu we have to work
    around the not yet available config file.  Even if I regard this
    as a bug in menu, we have to handle this here vor the moment.
    Closes: #484167

 -- Andreas Tille <tille@debian.org>  Mon, 05 May 2008 12:56:37 +0200

cdd (0.5.1) unstable; urgency=low

  * Changed the Maintainer to Custom Debian Distribution List
    <debian-custom@lists.debian.org>
  * Move debian/README.source into every meta package in case such
    a file exists
    Closes: #479249
  * added dh_lintian
    Closes: #479248

 -- Andreas Tille <tille@debian.org>  Sun, 04 May 2008 09:41:03 +0200

cdd (0.5.0) unstable; urgency=low

  * Removed VERSION file (which is hard to maintain) and
    teach debian/rules to obtain version from debian/changelog
    to build dist target
  * devtools/rules:
    - Added dist target to build source distribution tarball
  * debian/rules:
    several fixes to make use of make variables instead of
    shell variables
  * The source tarball of a CDD is now builded with prebuilded
    debian/control file and is not rebuilded per package build
    process.
  * Added "dist" target to devtools/Makefile which builds
    "get-orig-source" target of devtools/rules.  If CDDs
    build their source tarball using these tools nothing
    has to be cleaned in build process
    Closes: #468346

 -- Andreas Tille <tille@debian.org>  Fri, 07 Mar 2008 13:23:33 +0100

cdd (0.4.8) unstable; urgency=low

  * Small fix to allow more than one binary package in control.stub
  * Added myself to uploaders.

 -- José L. Redrejo Rodríguez <jredrejo@debian.org>  Tue, 26 Feb 2008 14:07:45 +0100

cdd (0.4.7) unstable; urgency=low

  * devtools/rules:
    - Added '-a' / '-i' options to binary-{arch,indep} targets
    - Added a test whether we build for arch all or any around
      cdd-install-helper which is rather a quick hack than a
      real solution but works.  More discussion about what is
      needed has to be done.
  * debian/control: Build-Depends -> Build-Depends-Indep
  * templates/post{inst,rm}: Check existence of CDD configuration
    files before executing user menu code.  If no such config
    files exist there is no need to run this code

 -- Andreas Tille <tille@debian.org>  Tue, 19 Feb 2008 11:24:50 +0100

cdd (0.4.6) unstable; urgency=low

  * Fixed Vcs-Browser, Vcs-Svn (Thanks to Thijs Kinkhorst
    <thijs@debian.org>)
  * Fix templates/post{inst,rm} to check for files from
    cdd-common before accessing them

 -- Andreas Tille <tille@debian.org>  Thu, 14 Feb 2008 18:08:57 +0100

cdd (0.4.5.1) unstable; urgency=low

  * devtools/rules: Also build binary-arch packages because
    Debian Edu needs this to have correct dependencies on all
    available architectures.

 -- Andreas Tille <tille@debian.org>  Fri, 25 Jan 2008 08:55:59 +0100

cdd (0.4.5) unstable; urgency=low

  * Add debconf templates for better menu handling
  * Regard CDD-config.postinst.stub if user provides such a file
  * share/cdd/unixgroups/cdd-actions: verify whether a group
    that should be added just exists
  * share/menu/cdd-menu: Prepend "!C menu-1" before each menu entry
    that does not contain a "!C" line to enable menu entries in
    menu-2 format
  * Added Vcs-Browser and Vcs-SVN tags
  * Standards-Version: 3.7.3 (no changes needed)
  * devtools/cdd-install-helper: removed bashism
  * devtools/rules: removed bashisms

 -- Andreas Tille <tille@debian.org>  Sat, 20 Oct 2007 12:22:55 +0200

cdd (0.4.4) unstable; urgency=low

  * Remove old useless comment in share/menu/cdd-menu
  * If there is no tasksel package wanted, just obtain the
    meta package prefix from the CDD short name
  * Make use of cdd-get-names also in cdd-gen-control script

 -- Andreas Tille <tille@debian.org>  Fri, 19 Oct 2007 16:12:51 +0200

cdd (0.4.3) unstable; urgency=low

  * Add a newline after adding config/control to debian/control
  * If a CDD provides an auto-apt helper name it CDD-config instead
    of CDD-common

 -- Andreas Tille <tille@debian.org>  Thu, 18 Oct 2007 20:14:16 +0200

cdd (0.4.2) unstable; urgency=low

  * Build-Depends: texlive-latex-base, texlive-latex-extra,
    texlive-fonts-recommended, texlive-latex-recommended
    Closes: #445777
  * debian/cdd-dev.lintian.overrides: Renamed common to config

 -- Andreas Tille <tille@debian.org>  Mon, 08 Oct 2007 12:02:41 +0200

cdd (0.4.1) unstable; urgency=low

  * Moved documentation package cdd-doc into same source package
  * Reworked debian/copyright
  * devtools/cdd-install-helper: If "Task:" keyword is missing in
    a task control file just do not put it into tasksel area under
    /usr/share/cdd/tasks/CDD

 -- Andreas Tille <tille@debian.org>  Fri, 31 Aug 2007 19:01:52 +0200

cdd (0.4) unstable; urgency=low

  * New upstream version that cdd-gen-control in favour of a newly
    adopted gen-control from Debian-Edu 0.821.
    (Please see the upstream changelog for all changes)
    Closes: #436831

 -- Andreas Tille <tille@debian.org>  Tue, 21 Aug 2007 20:56:39 +0200

cdd (0.3.11.1) unstable; urgency=low

  * Remove Bashism (source --> .)
    Closes: #394604 (of med-common, this bug did not really belong to
                     med-common but was caused by the bashism in cdd-common)
  * Standards-Version: 3.7.2 (no changes necessary)
  * Build-Depends: debhelper (>= 5)

 -- Andreas Tille <tille@debian.org>  Mon, 23 Oct 2006 07:46:58 +0200

cdd (0.3.11) unstable; urgency=low

  * Fix problem with resulting *.dsc files that did not list the
    files that were actually created in the Binary line
  * No changes necessary Standards-Version: 3.6.2
  * Removed: Conflicts: med-common (<= 0.4); which was never released
    in stable
  * Because there is no direct use of debconf in the cdd package but
    debconf is used in the package builded using cdd-dev the ${misc:Depends}
    variable does not really work.  That's why we introduce an explicite
      Depends: debconf (>= 0.5) | debconf-2.0
  * Increased dependant menu version from 2.1.12 to 2.1.25
  * Adapted lintian.overrides to reflect the change in menu
    (files now in /usr/share/menu instead of /usr/lib/menu)

 -- Andreas Tille <tille@debian.org>  Sun, 25 Sep 2005 21:36:29 +0200

cdd (0.3.10) unstable; urgency=low

  * Andreas Tille
    - Fixed some man pages and examples
    - Fixed bugs which showed up in Debian-Junior packaging
      (work around missing final newlines in several places,
       fix problem with menu items which are not using '"'
       around the entries)
  * Guillem Jover
    - Added Catalan (ca) debconf translation.
  * Otavio Salvador
    - cdd-gen-control:
      o Add -D suport to cdd-gen-control to degrade dependencies to
        recommends;
      o Add support for architecture dependent packages;
      o Add fallback to architecture independent packages so allow
        backward compatibility;
      o Add suport to tasksel based tasks;
    - task-files: added.
  * Adopted to findutils 4.2.22: specify the -maxdepth option before
    non-option arguments

 -- Andreas Tille <tille@debian.org>  Sat, 21 Aug 2004 09:06:13 +0200

cdd (0.3.9) experimental; urgency=low

  * Otavio Salvador
    - Move the project to Subversion.
  * Andreas Tille
    - Exclude .svn dirs in dist target of debian/rules
    - Removed Provides/Replaces/Conflicts: med-common-dev
      because this package vanished completely from the archive now
      and never reached stable.
    - Moved cdd-menu from /usr/lib/menu to /usr/share/menu
    - User menus now are created from installed dependency menus
      if they are not explicitely overriden by the meta package maintainer

 -- Andreas Tille <tille@debian.org>  Thu, 15 Jul 2004 11:13:22 +0200

cdd (0.3.4) unstable; urgency=high

  * Fixed common.postinst template which finally fixed bug #259412.
  * Removed contrib non-free and non-US from the templates - we want
    to build packages for main by default, others should be added
    manually by local maintainers.
  * Urgency set to high because it *really* fixes a grave bug.

 -- Andreas Tille <tille@debian.org>  Thu, 15 Jul 2004 10:51:44 +0200

cdd (0.3.3) unstable; urgency=high

  * Remove broken code from templates/postinst to clean up
    ${HOME}/.menu directories.  This code does more harm than
    it would help.
    Closes: #259412
  * Urgency set to high because it fixes a grave bug.

 -- Andreas Tille <tille@debian.org>  Wed, 14 Jul 2004 21:34:55 +0200

cdd (0.3.2) unstable; urgency=low

  * Remove ${HOME}/.menu/cdd-menu in postinst only if ${MENU}/.menu exists
  * Added /etc/apt/apt.conf.d script to #cdd#-common templates to invoke
    cdd-update-usermenus after all meta packages of a CDD were installed
    if requested by a shared debconf variable
  * Use #CDDNAME# variable in debconf templates of cdd-dev.  This variable
    can be set either in common/conf or it is builded by "Debian-Cdd".
  * cdd-install-helper handles CDD-common.{config,template}
  * Usage of get-group-users deprecated, use cdd-tools instead.
    Moved functionality of get-group-users to unixgroups/cdd-actions.
  * Enhanced example directory.
  * Otavio Salvador
    - Add support to Pre-Depends field in cdd-gen-control script.
    - Change the default task prefix from 'education-' to 'test-'.
    - Include code to stop if exist a task with name 'common'.
  * Andre Luis Lopes
    - Added Brazilian Portuguese (pt_BR) cdd-dev debconf template
      translation.

 -- Andreas Tille <tille@debian.org>  Wed,  9 Jun 2004 08:20:26 +0200

cdd (0.3.1) unstable; urgency=low

  * Do not change ${HOME}/.menu any more as it was done in 0.3
    Thanks to Cosimo Alfarano <kalfa@debian.org>
  * Provide man page for cdd.conf
  * Added newline at end of examples/common/control
    Closes: #251889

 -- Andreas Tille <tille@debian.org>  Mon, 31 May 2004 22:45:56 +0200

cdd (0.3) unstable; urgency=low

  * Cosimo Alfarano <kalfa@debian.org> did some major rewriting
    to make the scripts much more flexible (many thanks to Cosimo).
  * cdd-inst-helper now adds versioned med-common dependency if menu
    is created by this package.
  * templates/post{inst,rm} feature user menu creation if variable
    UPDATEUSERMENU is set to "yes"

 -- Andreas Tille <tille@debian.org>  Tue, 13 Apr 2004 21:51:01 +0200

cdd (0.2) unstable; urgency=low

  * Initial release of this package which contains code from several
    packages but should be of common use now.
    Closes: #240243
  * Renamed med-common-dev to cdd-dev because it is intended to work
    for all Custom Debian Distributions now.
  * Renamed med-common to cdd-common.
    (old changelog is available in changelog.med-common.
  * Adopted gen-control from debian-edu-0.768

 -- Andreas Tille <tille@debian.org>  Tue, 23 Mar 2004 18:41:20 +0100

med-common-dev (0.1-5) unstable; urgency=low

  * Standards-Version: 3.6.1.0
  * Fixed typo in debian/control
    closes: #221212
    While I think that also spelling bugs should be reported and fixed
    I really wonder why people set the priority of those bugs to
    "normal" instead of "wishlist".

 -- Andreas Tille <tille@debian.org>  Tue, 18 Nov 2003 08:58:14 +0100

med-common-dev (0.1-4) unstable; urgency=low

  * really installed the examples to /usr/share/doc/med-common-dev/examples

 -- Andreas Tille <tille@debian.org>  Mon,  9 Sep 2002 22:19:53 +0200

med-common-dev (0.1-3) unstable; urgency=low

  * fixes a really stupid bug in med_install_helper

 -- Andreas Tille <tille@debian.org>  Wed, 26 Jun 2002 21:06:11 +0200

med-common-dev (0.1-2) unstable; urgency=low

  * med_install_helper script now cares for an /etc/med/menu
    directory if it was not builded in debian/rules and prints out
    a warning if no menu is provided

 -- Andreas Tille <tille@debian.org>  Wed, 26 Jun 2002 15:59:26 +0200

med-common-dev (0.1-1) unstable; urgency=low

  * Initial release
    closes: #150938

 -- Andreas Tille <tille@debian.org>  Mon, 24 Jun 2002 17:19:38 +0200
