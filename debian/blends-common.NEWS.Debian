blends (0.6.9) unstable; urgency=low

  Do not try to take over config files from /etc/cdd -
  it just leads to trouble and is not worth the effort
  because they are unchanged in most practical cases
  --> Please check the files in /etc/cdd if existent and
      take over the changes manually if needed to
      /etc/blends

 -- Andreas Tille <tille@debian.org>  Mon, 11 Jan 2010 19:23:04 +0100

blends (0.6.6) unstable; urgency=low

  * Previous versions of blends-common tried to convert config files from
    CDDs in /etc/cdd/<CDDNAME> installed on the system to the new location
    /etc/blends/<BLENDS> to enable a smooth migration.  Unfortunately this
    is a violation of policy 10.7.3, see 
    http://www.debian.org/doc/debian-policy/ch-files.html#s10.7.3,
    because it conflicts with the default config files in the config
    package of a Blend so this has to be dropped.  It concerns only
    three Blends and the change is very minimal (if needed ever - you
    are probably doing better with the new default configuration file).
    
    For the sake of completenes the new blend configuration directory has
    a copy of the old configuration file with the extension *.cdd to make
    sure that nothing is lost.  You should inspect this file and can
    safely remove it afterwards.

 -- Andreas Tille <tille@debian.org>  Fri, 21 Aug 2009 14:24:50 +0200

blends (0.6.0) unstable; urgency=low

  * The great renaming:  The confusing name Custom Debian Distribution
    was dropped after DebConf 8 and replaced by Debian Pure Blends -
    in short blends if the Debian internal use is evident.  This
    reflects in a rename of the package and all the tools.
  * This change results in a rename of the cdd-dev binary package to
    blends-dev and cdd-commons binary package to blends-common.
    To provide a reasonable migration path dummy packages cdd-dev and
    cdd-common are provided as well which contain symlinks to the tools
    in the respective blends package to make sure all projects are able
    to migrate their meta packages smoothly.
  * The projects affected are:
    - Debian Edu
    - Debian Junior
    - Debian Med
    - Debian Science
                
 -- Andreas Tille <tille@debian.org>  Sun, 19 Oct 2008 15:23:01 +0200

cdd (0.3.1) unstable; urgency=low

  * In cdd 0.3 user menus where implemented by placing a symlink cdd-menu
    in the .menu directory of each user who belongs to any Custom
    Debian Distrinution role.  Now a solution was found which does not
    touch users .menu directory.

    To avoid code for removal of these old cdd-menu links in the users
    home directories in the future postinst scripts it seems to be the
    best solution to ask the local administrator do remove these links
    manually.  So please remove all symlinks named

       <userhome>/.menu/cdd-menu

 -- Andreas Tille <tille@debian.org>  Thu, 17 May 2004 17:42:38 +0200
