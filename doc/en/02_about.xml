<chapter id="about">

  <title>What are Debian Pure Blends?</title>

<sect1 id="debian">
  <title>What is Debian?</title>

<para>
 The core of an operating system is a piece of software that interacts
 with the hardware of the computer, and provides basic functionality
 for several applications.  On Linux based systems, the so-called
 kernel provides this functionality, and the term Linux just means
 this core without those applications that provide the functionality
 for users.  Other examples are the Hurd, or the flavours of the BSD
 kernel.
</para>

<para>
 Many applications around UNIX-like kernels are provided by the 
 <ulink url="http://www.gnu.org/">GNU</ulink> system.  That is why Linux based
 operating systems are described as GNU/Linux systems.  The GNU tools
 around the Linux kernel build a complete operating system.
</para>

<para>
 Users do not need only an operating system.  They also need certain
 applications like web servers, or office suites.  A
 <emphasis>distribution</emphasis> is a collection of software packages around the
 GNU/Linux operating system that satisfies the needs of the target
 user group.  There are general distributions, which try to support
 all users, and there are several specialised distributions, which
 each target a special group of users.
</para>

<para>
 <emphasis>Distributors</emphasis> are those companies that are building these
 collections of software around the GNU/Linux operating system.
 Because it is Free Software, the user who buys a distribution pays
 for the service that the distributor is providing.  These services
 might be:
 
<itemizedlist>
  <listitem>
    <para>Preparing a useful collection of software around GNU/Linux.</para>
  </listitem>
  <listitem><para>Caring for smooth installation that the target user is able to
         manage.</para>
  </listitem>
  <listitem>
      <para>Providing software updates and security fixes.</para>
  </listitem>
  <listitem>
      <para>Writing documentation and translations to enable the user to use
         the distribution with maximum effect.</para>
  </listitem>
  <listitem><para>Selling Boxes with ready to install CDs and printed
         documentation.</para>
  </listitem>
  <listitem>
      <para>Offering training and qualification.</para>
  </listitem>
 </itemizedlist>

</para>

<para>
 Most distributors ship their distribution in binary packages.  Two
 package formats are widely used:
 
 <variablelist>
  <varlistentry>
    <term>RPM (RedHat Package Manager)</term>
    <listitem>
      <para>which is supported by RedHat, SuSE and others.</para>
    </listitem>
  </varlistentry>
  
  <varlistentry>
  <term>DEB (Debian Package)</term>
    <listitem>
      <para>used by Debian and derived distributions.</para>
    </listitem>
  </varlistentry>
 </variablelist>

 All GNU/Linux distributions have a certain amount of common ground,
 and the <ulink url="http://www.linuxbase.org/">Linux Standard Base</ulink> 
 (LSB) is attempting to develop and promote a set of standards
 that will increase compatibility among Linux distributions, and enable
 software applications to run on any compliant system.
</para>

<para>
 The very essence of any distribution, (whether delivered as RPMs, DEBs,
 Source tarballs or ports) is the choice of <emphasis>policy statements</emphasis>
 made (or not made, as the case may be) by the creators of the distribution.
</para>

<para>
 <emphasis>Policy statements</emphasis> in this sense are things like
 "configuration files live in
 <filename>/etc/$package/$package.conf</filename>, logfiles go to
 <filename>/var/log/$package/$package.log</filename> and the documentation
 files can be found in <filename>/usr/share/doc/$package</filename>."
</para>

<para>
 The policy statements are followed by the tool-chains and
 libraries used to build the software, and the lists of dependencies, which
 dictate the prerequisites and order in which the software has to be
 built and installed. (It's easier to ride a bicycle if you put the wheels
 on first. ;-) )
</para>

<para>
 It is this <emphasis>adherence to policy</emphasis> that causes a distribution
 to remain consistent within its own bounds. At the same time, this is
 the reason why packages can not always be safely installed across
 distribution boundaries. A SuSE <filename>package.rpm</filename> might not
 play well with a RedHat <filename>package.rpm</filename>, although the
 packages work perfectly well within their own distributions. A
 similar compatability problem could also apply to packages from the
 same distributor, but from a different version or generation of the
 distribution.
</para>

<para>
<remark>AT: The context is somehow missing here.</remark>
 As you will see later in more detail, Debian Pure Blends are
 just a modified ruleset for producing a modified (specialised)
 version of Debian GNU/Linux. 
</para>
<para>
 A package management system is a very strong tool to manage software
 packages on your computer. A large amount of the work of a
 distributor is building these software packages.
</para>

<para>
 Distributors you might know are 
  <ulink url="http://www.redhat.com/">RedHat</ulink>, 
  <ulink url="http://www.suse.com/">SuSE</ulink>,
  <ulink url="https://www.ubuntu.com/">Ubuntu</ulink>
  and others.
</para>
<para>
 <ulink url="http://www.debian.org">Debian</ulink> is just one of them.
</para>

<para>
Well, at least this is what people who do not know Debian well might
think about it.  But, in fact, Debian is a different kind of
distribution ...
</para>

</sect1>

<sect1 id="whatdebian">
  <title>What is Debian? (next try)</title>

<para>
The Debian Project is an association of individuals who have made
common cause to create a free operating system. This operating system
that we have created is called <emphasis>Debian GNU/Linux</emphasis>,
or simply Debian for short.
</para>
<para>
Moreover, work is in progress to provide Debian of kernels other than
Linux, primarily for the Hurd.  Other possible kernels are the
flavours of BSD, and there are even people who think about ports to MS
Windows.
</para>
<para>
All members of the Debian project are connected in a 
<ulink url="http://people.debian.org/~tille/talks/img/earthkeyring.png">web of trust</ulink>,
which is woven by signing GPG keys.  One requirement to become a
member of the Debian project is to have a GPG key signed by a Debian
developer.  Every time one Debian developer meets another developer
for the first time, they sign each other's keys.  In this way, the web
of trust is woven.
</para>

</sect1>

<sect1 id="difference">
  <title>Differences from other distributions</title>

<para>

<itemizedlist>
  <listitem>
    <para>Debian is not a company, but an organisation.</para>
  </listitem>
  <listitem>
    <para>It does not sell anything.</para>
  </listitem>
  <listitem>
    <para>Debian members are volunteers.</para>
  </listitem>
  <listitem>
    <para>Maintainers are working on the common goal:
     to build the best operating system they can achieve.
    </para>
   </listitem>
  <listitem>
    <para>Debian maintains the largest collection of ready-to-install Free
        Software on the Internet.</para>
  </listitem>
  <listitem>
    <para>There are two ways to obtain Debian GNU/Linux:</para>
     <orderedlist>
      <listitem>
        <para>Buy it from some <emphasis>other</emphasis> distributor on
          CD. Perhaps the correct term would be
          <emphasis>re</emphasis>distributor. Because Debian is free, anybody can
          build his own distribution based on it, sell CDs, and even
          add new features, such as printed documentation, more software,
          support for different installers and more.
        </para>
      </listitem>
      <listitem>
        <para>Download Debian from the web for free.</para>
      </listitem>
     </orderedlist>
       <para>The latter is the common way, and there are really great tools
       to do it this way.  Certainly it is always possible to copy Debian
       from a friend.
     </para>
  </listitem>
</itemizedlist>

</para>
 </sect1>

<sect1 id="Blends">
  <title>Debian Pure Blends</title>

<para>
Debian contains nearly 22.000 binary packages, and this number is
constantly increasing.  There is no single user who needs all these
packages (even if conflicting packages are not considered).
</para>
<para>The normal user is interested in a subset of these packages.  But
how does the user find out which packages are really interesting?
</para>
<para>
One solution is provided by the <package>tasksel</package> package.
It provides a reasonable selection of quite general tasks that can be
accomplished using a set of packages installed on a Debian GNU/Linux
system.  But this is not really fine grained, and does not address all
of the needs of user groups with special interests.
</para>
<para>
<emphasis>Debian Pure Blends</emphasis> - in short Blends if used clearly in the
Debian internal context which makes "Pure" and "Debian" obvious -
which were formerly known as Custom Debian Distributions (this name
was confusing because it left to much room for speculation that this
might be something else than Debian) try to provide a solution for
<emphasis>special groups of target users with different skills and
interests</emphasis>. Not only do they provide handy collections of specific
program packages, but they also ease installation and configuration
for the intended purpose.
</para>
<para>
Debian Pure Blends are <emphasis>not forks</emphasis> from Debian.  As the
new name says clearly they are pure Debian and just provide a specific
flavour.  So if you obtain the complete Debian GNU/Linux distribution,
you have all available Debian Pure Blends included.
</para>
<para>
The concept of what is called <emphasis>Blend</emphasis> in Debian is also known
in other distributions.  For instance in Fedora there are
<ulink url="http://fedoraproject.org/wiki/SIGs">Special Interest Groups (SIGs)</ulink>
even if some SIGs in Fedora are what in Debian is known as internal
project because it is focussed on technical implementations and not
on user-oriented applications.
</para>
</sect1>

<sect1 id="remastered">
  <title>Difference between a Blend and a remastered system</title>

<para>
Not necessarily all currently existing Blends are actually providing
installation media (live media or installer).  The reason for this is
that such installation media are not always necessary / wanted.  You
can just install plain Debian and install some metapackages on top of it.
However, the metapackage approach makes the creation of installation
media quite simple by using
<ulink url="http://live.debian.net/">Debian Live</ulink>.
Here are some reasons for this approach compared to a remastering
strategy.
</para>

<sect2>
  <title>Technical</title>
  <para>
  The process for creation of a blend involves starting with a Debian or
  derivative repository and creating an image directly from that (live,
  install or otherwise) that contains a selection of material from that
  repository delivered in such a way that it is usable by a particular
  target user for a particular purpose with a minimum of effort.
  </para>
  <para>
  By contrast, the process of remastering generally involves first
  downloading an image produced by the parent distro (live, install or
  otherwise,) then tearing it apart and reassembling it with your
  customizations applied.
  </para>
</sect2>

<sect2>
    <title>Philosophical</title>
    <para>
    The blends philosophy is to work as closely with the parent distro as
    possible. If possible, the project should be done entirely within the
    distro as a subproject, containing only material supplied by the parent
    distro. We call this a "Pure Blend".
    </para>
    <para>
    The remastering philosophy (if it can be called that) seems to be
    "whatever works" and involves little or no interaction with the parent
    distro. It's a lazy approach used by people who have newly discovered
    that they can hack images to make them into custom images to make
    something uniquely theirs. Probably fine for quick-and-dirty results,
    but hard to support in the long run.
    </para>
    <para>
    The users of a blend are served better than the users of a
    remaster because of the following advantages:
    </para>

    <sect3>
      <title>Technical advantage</title>
      <para>
      A new version of a well-crafted blend ought to be able to be produced at
      any time directly from the repository simply by building it; the user
      has some assurance that the resulting system remains 'untainted' by
      hacking it up with scripts that 'damage' the original system by removing
      files from packages, changing files in packages, etc. something that
      hurts maintainability / support for such a system.
      </para>
    </sect3>

    <sect3>
      <title>Community advantage</title>
      <para>
      A blend project aims to leverage support resources from the existing
      community to serve some sub-community within it. They accomplish this by
      not violating Debian packaging policy, producing something that is
      either pure Debian (a "pure blend") or Debian + additional packages,
      rather than some frankendistro artlessly stitched together from someone
      else's distro with scripts that change things everywhere with no regard
      to policy. Thus, normal support channels can be used with a pure blend
      since what you end up with is not a derivative at all, but just Debian,
      set up and ready to go for whatever you wanted to use it for.
      </para>
    </sect3>

</sect2>

</sect1>

<sect1 id="resources">
  <title>Further resources about Blends</title>
<para>
<variablelist>

  <varlistentry>
  <term>Wiki</term>
   <listitem><para><ulink url="https://wiki.debian.org/DebianPureBlends"></ulink></para></listitem>
  </varlistentry>

  <varlistentry>
  <term>Mailing list</term>
   <listitem><para><ulink url="https://lists.debian.org/debian-blends/"></ulink></para></listitem>
  </varlistentry>

  <varlistentry>
  <term>IRC #debian-blends</term>
   <listitem><para><ulink url="irc://irc.debian.org/debian-blends"></ulink></para></listitem>
  </varlistentry>
</variablelist>
</para>
</sect1>

</chapter>
